package generator;

import java.util.ArrayList;
import java.util.List;

import model.Point;

public class Generator {
    public static List<Point> generatePointsList(){

        double radius = 50;

        List<Point> points = new ArrayList<>();
        points.add(new Point("Home",51.234555,22.546391, radius));//home
        points.add(new Point("Kebab",51.234522, 22.543831, radius));//kebab
        points.add(new Point("Library", 51.236041, 22.550993, radius));//library
        points.add(new Point("50m",51.234585, 22.546780, radius));//50m
        return points;
    }

}
