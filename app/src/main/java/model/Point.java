package model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Point implements Parcelable {
    private double longitude;
    private double lattitude;
    private double radius;
    private boolean flag;
    private String title;

    public Point(String title, double lattitude, double longitude, double radius) {
        this.title = title;
        this.longitude = longitude;
        this.lattitude = lattitude;
        this.radius = radius*0.00001*1.4;
        flag = false;
    }

    public String getTitle() {
        return title;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return lattitude;
    }

//    public void setFlagTrue(){
//        flag = true;
//    }

    public boolean getFlag() {
        return flag;
    }

    public void verifyFlag(Location location){

        if (!flag) {
            double xc = lattitude;
            double yc = longitude;

            double x1 = location.getLatitude();
            double y1 = location.getLongitude();

            double D = Math.sqrt((x1 - xc) * (x1 - xc) + (y1 - yc) * (y1 - yc));
            if (D < radius) {
                flag = true;
            }
            Log.d("my_tag", "D: "+D+ " R: "+radius);
        }
        Log.d("my_tag", "verifyFlag: "+ flag);

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.lattitude);
        dest.writeDouble(this.radius);
        dest.writeByte(this.flag ? (byte) 1 : (byte) 0);
    }

    protected Point(Parcel in) {
        this.title = in.readString();
        this.longitude = in.readDouble();
        this.lattitude = in.readDouble();
        this.radius = in.readDouble();
        this.flag = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Point> CREATOR = new Parcelable.Creator<Point>() {
        @Override
        public Point createFromParcel(Parcel source) {
            return new Point(source);
        }

        @Override
        public Point[] newArray(int size) {
            return new Point[size];
        }
    };
}
