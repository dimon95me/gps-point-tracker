package com.example.gpssender;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindViews;
import butterknife.ButterKnife;
import model.Point;
import service.MyLocationService;

public class MapPointsEditActivity extends AppCompatActivity {

    private static final String TAG = "my_tag";

    @BindViews({R.id.map_edit_point_one_text, R.id.map_edit_point_two_text, R.id.map_edit_point_three_text, R.id.map_edit_point_four_text})
    List<EditText> titles;

    @BindViews({R.id.map_edit_point_one_text_longitude, R.id.map_edit_point_two_text_longitude, R.id.map_edit_point_three_text_longitude, R.id.map_edit_point_four_text_longitude})
            List<EditText> longitudes;

    @BindViews({R.id.map_edit_point_one_text_lattitude, R.id.map_edit_point_two_text_lattitude, R.id.map_edit_point_three_text_lattitude, R.id.map_edit_point_four_text_lattitude})
            List<EditText> lattitudes;

    List<Point> mPoints;

    ServiceConnection mServiceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_points_edit);
        ButterKnife.bind(this);

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                Log.d(TAG, "onServiceConnected: from points editor");
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.d(TAG, "onServiceDisconnected: from points editor");
            }
        };

        showDataFromPoints();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_map_points_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()== R.id.map_points_menu_yes){
            writeDataToPoints();
//            Intent intent = new Intent(this, MapActivity.class);
//            intent.putParcelableArrayListExtra("return_from_editing_points", (ArrayList<? extends Parcelable>) mPoints);
//            setResult(RESULT_OK, intent);
            Intent intent = new Intent(this, MyLocationService.class);
            intent.putParcelableArrayListExtra("return_from_editing_points", (ArrayList<? extends Parcelable>) mPoints);
            bindService(intent, mServiceConnection, 0);
//            unbindService(mServiceConnection);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDataFromPoints() {
        if(getPoints()) {
        for (int i = 0; i < mPoints.size(); i++) {
            titles.get(i).setText(mPoints.get(i).getTitle());
            longitudes.get(i).setText(String.valueOf(mPoints.get(i).getLongitude()));
            lattitudes.get(i).setText(String.valueOf(mPoints.get(i).getLatitude()));
        }
        }

    }

    private void writeDataToPoints() {
            for (int i = 0; i < mPoints.size(); i++) {
                mPoints.get(i).setTitle(titles.get(i).getText().toString());
                mPoints.get(i).setLongitude(Double.valueOf(longitudes.get(i).getText().toString()));
                mPoints.get(i).setLattitude(Double.valueOf(lattitudes.get(i).getText().toString()));
            }
        Toast.makeText(this, mPoints.get(3).getTitle(), Toast.LENGTH_SHORT).show();
    }

    private Boolean getPoints() {
        if (getIntent().getParcelableArrayListExtra("point_list_floating_intent") != null) {
            mPoints = getIntent().getParcelableArrayListExtra("point_list_floating_intent");
//            Log.d("my_tag", "MapPointsEditActivity onCreate: " + mPoints.get(1).getTitle());
            return true;
        } else{
            Log.d("my_tag", "MapPointsEditActivity onCreate: is null");
            return false;
        }
    }

}
