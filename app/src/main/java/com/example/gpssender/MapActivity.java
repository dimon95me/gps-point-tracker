package com.example.gpssender;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import model.Point;
import service.MyLocationService;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final float DEFAULT_ZOOM = 15f;
    private static final String TAG = "my_tag";
    private static final int REQUEST_CODE_1 = 1;
    private GoogleMap mMap;

    private BroadcastReceiver broadcastReceiver;
    private Location currentLocation;

    @BindView(R.id.map_floating_action)
    FloatingActionButton floatingButton;
    @BindView(R.id.map_relative_layout)
    RelativeLayout mapRelativeLayout;
    @BindView(R.id.map_action_search_left_points)
    ImageView imageSearch;
    @BindView(R.id.map_text_lefr_point)
    TextView leftPointText;

    List<Point> points;

    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    floatingButton.show();

                    Location location = intent.getParcelableExtra("location");
                    currentLocation = location;
                    Log.d(TAG, "onReceive onMapActivity: " + currentLocation.toString());
//                    if (points == null) {
                        points = intent.getParcelableArrayListExtra("point_list");
//                    }
                    Log.d(TAG, "onReceive: points"+points.get(1).getTitle());
                    Log.d(TAG, "onReceive: points"+points.get(2).getTitle());
                    Log.d(TAG, "onReceive: points"+points.get(3).getTitle());
                    Log.d(TAG, "onReceive: points"+points.get(0).getTitle());
                    if (points != null ) {
                        createMarkers(points);
                    }
                    leftPointText.setText("Left points: " + pointsLeft());
                    if (allPointsChanged()) {
                        mapRelativeLayout.setBackgroundResource(R.drawable.green_successfull_border);
                        leftPointText.setTextColor(Color.WHITE);
                        leftPointText.setText("Welcome home!!!");

                        stopService(new Intent(getApplicationContext(), MyLocationService.class));
                        if (broadcastReceiver != null) {
                            unregisterReceiver(broadcastReceiver);
                        }

                        whatToDoTheNext();
                    }
                    moveCamera(location, DEFAULT_ZOOM);

                }
            };
            registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
        }
    }

    private void whatToDoTheNext() {
        Toast.makeText(this, "All points are checked. \nWell come home!!!", Toast.LENGTH_SHORT).show();
    }

    private boolean allPointsChanged() {
        boolean check = true;
        for (Point point : points) {
            if (!point.getFlag()) {
                check = false;
                break;
            }
        }
        return check;
    }

    @OnClick(R.id.map_floating_action)
    public void floatingActionOnClick() {
        Intent intent = new Intent(this, MapPointsEditActivity.class);
        if (points != null) {
            intent.putParcelableArrayListExtra("point_list_floating_intent", (ArrayList<? extends Parcelable>) points);
            startActivity(intent);
        } else {
            Log.d(TAG, "floatingActionOnClick: intent is not ready");
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        floatingButton.hide();
        imageSearch.setOnClickListener((View v) -> {
            try {
                for (Point point : points) {
                    if (!point.getFlag()) {
                        moveCamera(point, DEFAULT_ZOOM);
                        break;
                    }
                }
            } catch (NullPointerException e) {
                Toast.makeText(this, "Map is not ready yet", Toast.LENGTH_SHORT).show();
            }
        });

        startService(new Intent(getApplicationContext(), MyLocationService.class));
        init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_1:
                if (resultCode == RESULT_OK) {
                    if (getIntent().getParcelableArrayListExtra("return_from_editing_points") != null) {
                        points = getIntent().getParcelableArrayListExtra("return_from_editing_points");
                        Log.d(TAG, "onActivityResult: "+points.get(1).getTitle());
                        Log.d(TAG, "onActivityResult: points"+points.get(1).getTitle());
                        Log.d(TAG, "onActivityResult: points"+points.get(2).getTitle());
                        Log.d(TAG, "onActivityResult: points"+points.get(3).getTitle());
                        Log.d(TAG, "onActivityResult: points"+points.get(0).getTitle());
                        createMarkers(points);
                    }else{
                        Log.d(TAG, "onActivityResult: points is null");
                    }
                }
        }
    }

    @SuppressLint("MissingPermission")
    private void init() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync((GoogleMap gMap) -> {
            mMap = gMap;
            mMap.setMyLocationEnabled(true);

//            mMap.clear();

        });
    }

    private int pointsLeft() {
        int toVisit = 0;
        for (Point point : points) {
            if (!point.getFlag()) toVisit++;
        }
        return toVisit;
    }

    private void createMarkers(List<Point> points) {
        mMap.clear();
        for (Point point : points) {
            if (!point.getFlag()) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(new LatLng(point.getLatitude(), point.getLongitude()))
                        .title(point.getTitle());
                mMap.addMarker(markerOptions);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "Map is ready", Toast.LENGTH_SHORT).show();
        mMap = googleMap;
    }

    private void moveCamera(Location location, float zoom) {
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom( new LatLng(51.234585, 22.546780), DEFAULT_ZOOM));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM));

//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), zoom));
    }

    private void moveCamera(Point point, float zoom) {
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom( new LatLng(51.234585, 22.546780), DEFAULT_ZOOM));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(point.getLatitude(), point.getLongitude()), DEFAULT_ZOOM));

//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), zoom));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(getApplicationContext(), MyLocationService.class));
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }
}
