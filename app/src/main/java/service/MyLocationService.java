package service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import generator.Generator;
import model.Point;

public class MyLocationService extends Service {


    private static final String TAG = "my_tag";

    private LocationManager locationManager;
    private LocationListener listener;

    private List<Point> points;

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: service");
        points = Generator.generatePointsList();
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                checkPoints(location);


                Intent i = new Intent("location_update");
                i.putExtra("coordinates", location.getLatitude() + " " + location.getLongitude());
                i.putExtra("location", location);

//                if (equalLocations(location)) {
                    i.putParcelableArrayListExtra("point_list", (ArrayList<Point>) points);
//
                sendBroadcast(i);

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };


        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, listener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 0, listener);

        } else {
            Log.d("my_tag", "setUpLocationListener: location permissions does not granted");
        }
    }



    private boolean equalLocations(Location location) {
        boolean isListChanged = false;

        for (Point point : points) {
            if (!point.getFlag()) {
                point.verifyFlag(location);
                if (point.getFlag()) {
                    isListChanged = true;
                }
            }
        }

        return isListChanged;
    }

    private void checkPoints(Location location) {
        for (Point point : points) {
            point.verifyFlag(location);
        }
    }


    @Override
    public void onDestroy() {
        if (locationManager != null) {
            locationManager.removeUpdates(listener);
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        points = intent.getParcelableArrayListExtra("return_from_editing_points");
        return new Binder();
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        points = intent.getParcelableArrayListExtra("return_from_editing_points");
    }
}
